package com.chanisata;

public class Altar {
    private char altar;
    private int x;
    private int y;

    public Altar(char altar, int x, int y) {
        this.altar = altar;
        this.x = x;
        this.y = y;
    }

    public char getAltar() {
        return altar;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
