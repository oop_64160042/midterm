package com.chanisata;

public class App {
    public static void main(String[] args) {
        // Spec
        Spector bell = new Spector("Bell", "Female" ,'B', 8, 3, 3);
        Spector konkunk = new Spector("Konkunk", "Male" , 'K', 4, 1, 1);
        Spector heart = new Spector("Mr.Heart", "Male" , 'H', 9, 4, 5);
        // Sur
        Survivor tim = new Survivor("Tim", "Male" , 'T', 7, 0, 7);
        Survivor jen = new Survivor("Jen",  "Female" , 'J', 6, 1, 2);
        Survivor nipha = new Survivor("Nipha", "Female" , 'N', 5, 2, 1);
        Survivor don = new Survivor("Don", "Male" , 'D', 7, 6, 5);
        // Map
        Map map1 = new Map('_', 0 , 0);
        Map map2 = new Map('|', 3 , 1);
        Map map3 = new Map('|', 0 , 2);
        Map map4 = new Map('|', 0 , 3);
        Map map5 = new Map('_', 1 , 3);
        Map map6 = new Map('_', 2 , 3);
        Map map7 = new Map('_', 5 , 0);
        Map map8 = new Map('_', 6 , 0);
        Map map9 = new Map('|', 5 , 1);
        Map map10 = new Map('_', 1 , 0);
        Map map11 = new Map('_', 2 , 0);
        Map map12 = new Map('_', 3 , 0);
        Map map13 = new Map('_', 7 , 0);
        Map map14 = new Map('|', 7 , 1);
        Map map15 = new Map('|', 2 , 7);
        Map map16 = new Map('_', 5 , 6);
        Map map17 = new Map('|', 2 , 6);
        Map map18 = new Map('_', 7 , 6);
        Map map19 = new Map('_', 2 , 5);
        Map map20 = new Map('_', 6 , 6);
        Map map21 = new Map('_', 0 , 5);
        Map map22 = new Map('|', 7 , 2);
        Map map23 = new Map('|', 7 , 3);
        Map map24 = new Map('_', 6 , 3);
        Map map25 = new Map('_', 5 , 3);
        Map map26 = new Map('_', 0 , 8);
        Map map27 = new Map('_', 1 , 8);
        Map map28 = new Map('_', 2 , 8);
        Map map29 = new Map('|', 5 , 7);
        Map map30 = new Map('_', 5 , 8);
        Map map31 = new Map('_', 6 , 8);
        // Altar
        Altar altar1 = new Altar('#', 6 , 1);
        Altar altar2 = new Altar('#', 5 , 4);
        Altar altar3 = new Altar('#', 1 , 7);


        System.out.println("HOME SWEET HOME : Charector");

        System.out.println("-----------");
        System.out.println("Spector");
        System.out.println("-----------");
        bell.printName();       bell.printRating();
        konkunk.printName();    konkunk.printRating();
        heart.printName();      heart.printRating();
        System.out.println("-----------");
        System.out.println("Survivor");
        System.out.println("-----------");
        tim.printName();        tim.printRating();
        jen.printName();        jen.printRating();
        nipha.printName();      nipha.printRating();
        don.printName();        don.printRating();


        // แสดง Mr.Heart , Tim , Jen , Nipha และ Don บนแมพ
        System.out.println("-----------");
        System.out.println("Example, the positions of Mr.Heart , Tim , Jen, Nipha and Don on the map.");
        for (int y = 0; y <= 8; y++) {
            for (int x = 0; x <= 7; x++) {
                // Spect
                if (heart.getX() == x && heart.getY() == y) {
                    System.out.print(" " + heart.getSymbol() + " ");
                // Survi
                } else if (tim.getX() == x && tim.getY() == y) {
                    System.out.print(" " + tim.getSymbol() + " ");
                } else if (jen.getX() == x && jen.getY() == y) {
                    System.out.print(" " + jen.getSymbol() + " ");
                } else if (nipha.getX() == x && nipha.getY() == y) {
                    System.out.print(" " + nipha.getSymbol() + " ");
                } else if (don.getX() == x && don.getY() == y) {
                    System.out.print(" " + don.getSymbol() + " ");
                // altar
                } else if (altar1.getX() == x && altar1.getY() == y) {
                    System.out.print(" " + altar1.getAltar() + " ");
                } else if (altar2.getX() == x && altar2.getY() == y) {
                    System.out.print(" " + altar2.getAltar() + " ");
                } else if (altar3.getX() == x && altar3.getY() == y) {
                    System.out.print(" " + altar3.getAltar() + " ");
                // Map
                } else if (map1.getX() == x && map1.getY() == y) {
                    System.out.print(" " + map1.getMap() + " ");
                } else if (map2.getX() == x && map2.getY() == y) {
                    System.out.print(" " + map2.getMap() + " ");
                } else if (map3.getX() == x && map3.getY() == y) {
                    System.out.print(" " + map3.getMap() + " ");
                } else if (map4.getX() == x && map4.getY() == y) {
                    System.out.print(" " + map4.getMap() + " ");
                } else if (map5.getX() == x && map5.getY() == y) {
                    System.out.print(" " + map5.getMap() + " ");
                } else if (map6.getX() == x && map6.getY() == y) {
                    System.out.print(" " + map6.getMap() + " ");
                } else if (map7.getX() == x && map7.getY() == y) {
                    System.out.print(" " + map7.getMap() + " ");
                } else if (map8.getX() == x && map8.getY() == y) {
                    System.out.print(" " + map8.getMap() + " ");
                } else if (map9.getX() == x && map9.getY() == y) {
                    System.out.print(" " + map9.getMap() + " ");
                } else if (map10.getX() == x && map10.getY() == y) {
                    System.out.print(" " + map10.getMap() + " ");
                } else if (map11.getX() == x && map11.getY() == y) {
                    System.out.print(" " + map11.getMap() + " ");
                } else if (map12.getX() == x && map12.getY() == y) {
                    System.out.print(" " + map12.getMap() + " ");
                } else if (map13.getX() == x && map13.getY() == y) {
                    System.out.print(" " + map13.getMap() + " ");
                } else if (map14.getX() == x && map14.getY() == y) {
                    System.out.print(" " + map14.getMap() + " ");
                } else if (map15.getX() == x && map15.getY() == y) {
                    System.out.print(" " + map15.getMap() + " ");
                } else if (map16.getX() == x && map16.getY() == y) {
                    System.out.print(" " + map16.getMap() + " ");
                } else if (map17.getX() == x && map17.getY() == y) {
                    System.out.print(" " + map17.getMap() + " ");
                } else if (map18.getX() == x && map18.getY() == y) {
                    System.out.print(" " + map18.getMap() + " ");
                } else if (map19.getX() == x && map19.getY() == y) {
                    System.out.print(" " + map19.getMap() + " ");
                } else if (map20.getX() == x && map20.getY() == y) {
                    System.out.print(" " + map20.getMap() + " ");
                } else if (map21.getX() == x && map21.getY() == y) {
                    System.out.print(" " + map21.getMap() + " ");
                } else if (map22.getX() == x && map22.getY() == y) {
                    System.out.print(" " + map22.getMap() + " ");
                } else if (map23.getX() == x && map23.getY() == y) {
                    System.out.print(" " + map23.getMap() + " ");
                } else if (map24.getX() == x && map24.getY() == y) {
                    System.out.print(" " + map24.getMap() + " ");
                } else if (map25.getX() == x && map25.getY() == y) {
                    System.out.print(" " + map25.getMap() + " ");
                } else if (map26.getX() == x && map26.getY() == y) {
                    System.out.print(" " + map26.getMap() + " ");
                } else if (map27.getX() == x && map27.getY() == y) {
                    System.out.print(" " + map27.getMap() + " ");
                } else if (map28.getX() == x && map28.getY() == y) {
                    System.out.print(" " + map28.getMap() + " ");
                } else if (map29.getX() == x && map29.getY() == y) {
                    System.out.print(" " + map29.getMap() + " ");
                } else if (map30.getX() == x && map30.getY() == y) {
                    System.out.print(" " + map30.getMap() + " ");
                } else if (map31.getX() == x && map31.getY() == y) {
                    System.out.print(" " + map31.getMap() + " ");

                } else {
                    System.out.print("   ");
                }
            }
            System.out.println();
        }
    }
}
