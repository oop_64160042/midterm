package com.chanisata;

public class Map {
    private char map;
    private int x;
    private int y;

    public Map(char map, int x, int y) {
        this.map = map;
        this.x = x;
        this.y = y;
    }

    public char getMap() {
        return map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
