package com.chanisata;

public class Survivor {
    private String name;
    private String sex;
    private char symbol;
    private int rating;
    private int x;
    private int y;

    public Survivor(String name, String sex, char symbol, int rating, int x, int y) {
        this.name = name;
        this.sex = sex;
        this.symbol = symbol;
        this.rating = rating;
        this.x = x;
        this.y = y;
    }

    public void printName() {
        System.out.println(
                "Name :" + name + "  Sex :" + sex + "  Symbol :" + symbol + "  x :" + x + "  y :" + y);
    }

    public void printRating() {
        System.out.print(("Rating :"));
        for (int i = 0; i < rating; i++) {
            System.out.print("*");
        }
        System.out.println();
    }

    public String getName() {
        return name;
    }

    public char getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}
